CONTROLE DE TAREFAS                                 DUAS INTERFACES
_Criar nova tarefas
_Visualizar minhas tarefas
_Visualizar tarefas dos outros
_Mudar o responsável da tarefas
_Cadastrar tarefa


NOVA TAREFA                                         LISTA DE TAREFAS    <   REPOSITÓRIO DE TAREFAS [CLASSE]
[atributos]
- nome
- descrição
- responsável               [LISTA QUE INSTANCIE OBJETOS] []
- status                    [ENUM: In progress, Canceled, Completed] []


VISUALIZAR MINHAS TAREFAS [MÉTODO]
VISUALIZAR TAREFAS DOS OUTROS [MÉTODO]
MUDAR O RESPOSÁVEL DA TAREFA [MÉTODO]
CADASTRAR TAREFA [METODO]


# TASK [CLASS WITH ATTRIBUTES]                      - MODEL LAYER
# TASK REPOSITORY [CLASS WITH METHODS]              - REPOSITORY LAYER                  : ITASKREPOSITORY       (cadastrar tarefa)
# TASK BUSINESS [CLASS WITH METHODS]                - BUSINESS LAYER                    : ITASKBUSINESS         (demais atividades)


**IN TASK REPOSITORY, TO CREATE TWO LISTS: ONE FOR ME AND OTHER FOR GENERAL.


-------------------------------------


TEST LAYER

> CRIAR NOVA TAREFA ----------------- testar se foi criada, testar o responsável, testar o status
> VISUALIZAR MINHAS TAREFAS --------- testar coleção de tarefas
> VISUALIZAR TAREFAS DOS OUTROS ----- testar coleção de tarefas
> MUDAR O RESPONSÁVLE DA TAREFA ----- testar essa mudança
> CADASTRAR TAREFA ------------------ testar se foi cadastrada


-------------------------------------


STRUCTURE
> SLN --------------- TaskControlActivity
> CONSOLE ----------- TaskControl
> MSTEST ------------ TaskControlTest

# CONSOLE: 
- INTERFACES --------- ITaskRepository, ITaskManager
- MODEL -------------- Tasks
- REPOSITORY --------- TaskRepository                                           (or two repositorys? for me n' for general)
- MANAGER ------------ TaskManager, TaskView

# TEST:
- REPOSITORYTEST ----- TaskRepository
- MANAGERTEST -------- TaskManager
- VIEWTEST ----------- TaskView
