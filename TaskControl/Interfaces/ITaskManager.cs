public interface ITaskManager
{
    public string CreateNewTask(Tasks task);
    public void ChangeResponsibleAtRandom();
    public string ChangeResponsibleSpecifically(int taskIndex, int responsibleIndex);
}