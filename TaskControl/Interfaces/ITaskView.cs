public interface ITaskView
{
    public string ViewMyTasks();
    public string ViewOthersTasks();
}