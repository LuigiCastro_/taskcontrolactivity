public class TaskManager : TaskRepository, ITaskManager
{
    private readonly ITaskRepository _taskRepository;
    public TaskManager(ITaskRepository taskRepository)
    {
        _taskRepository = taskRepository;
    }

    public string CreateNewTask(Tasks task)
    {
        if(task == null)
        {
            throw new Exception("Value cannot be null");
        }
        if(task.Name.Count() <= 2 || task.Name == null)
        {
            throw new Exception("The name of the task is invalid!");
        }
        if(task.Description == null)
        {
            throw new Exception("The description of the task is invalid!");
        }
        if(task.Responsible_Task.Count() <= 2 || task.Responsible_Task == null)
        {
            throw new Exception("The name of responsible is invalid!");
        }
        if(task.TaskStatus == null || (double)task.TaskStatus < 0 || (double)task.TaskStatus > 2)
        {
            throw new Exception("The status of the task is invalid!");
        }
        
        RegisterTask(task);
        return logMessage;  
    }
    
    public void ChangeResponsibleAtRandom()
    {
        int i = 0;
        Random random = new();
        foreach(var task in tasksList)
        {
            int randomNumber = random.Next(responsiblesList.Count);
            tasksList[i].Responsible_Task = responsiblesList[randomNumber].Responsible_Name;
            // var newResponsible = tasksList[i].Responsible_Task;
            System.Console.WriteLine($"NEW RESPONSIBLE OF THE {tasksList[i].Name}: {tasksList[i].Responsible_Task}");

            // tasksList.Add(new Tasks {Responsible_Task = newResponsible});
            i++;
        }
        System.Console.WriteLine();
    }

    public string ChangeResponsibleSpecifically(int taskIndex, int responsibleIndex)
    {
        tasksList[taskIndex].Responsible_Task = responsiblesList[responsibleIndex].Responsible_Name;
        // tasksList.Add(new Tasks{ Responsible_Task = responsiblesList[responsibleIndex].Responsible_Name });
        System.Console.WriteLine($"NEW RESPONSIBLE OF THE {tasksList[taskIndex].Name}: {responsiblesList[responsibleIndex].Responsible_Name}\n");
        return responsiblesList[responsibleIndex].Responsible_Name;
    }
}

