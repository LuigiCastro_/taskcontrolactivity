public class TaskView : TaskRepository, ITaskView
{
    public string _task;
    private readonly ITaskRepository _taskRepository;
    public TaskView(ITaskRepository taskRepository)
    {
        _taskRepository = taskRepository;
    }
    public string ViewMyTasks()
    {
        int i = 0;
        foreach(var task in tasksList)
        {
            if(tasksList[i].Responsible_Task == "Luigi")
            {
                System.Console.WriteLine($"MY TASKS: {task.Name}, {task.Description}, {task.Responsible_Task}, {task.TaskStatus}");
                _task = tasksList[i].Name;
            }
            i++;
        }
        System.Console.WriteLine();
        // return tasksList.Where(responsible => responsible.Responsible_Task == "Luigi").ToString();
        return _task;
    }

    public string ViewOthersTasks()
    {
        int i = 0;
        foreach(var task in tasksList)
        {
            if(tasksList[i].Responsible_Task != "Luigi")
            {
                System.Console.WriteLine($"OTHERS TASKS: {task.Name}, {task.Description}, {task.Responsible_Task}, {task.TaskStatus}");
                _task = tasksList[i].Name;
            }
            i++;
        }
        System.Console.WriteLine();
        return _task;
    }
}