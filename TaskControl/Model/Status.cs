public class Status
{
    public enum TaskStatus
    {
        InProgress,
        Canceled,
        Completed,
    }
}