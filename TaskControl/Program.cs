﻿using Microsoft.Extensions.DependencyInjection;
using System.Linq.Expressions;
class Program
{
    static void Main(string[] args)
    {
        
        var serviceCollection = new ServiceCollection();
        serviceCollection.AddSingleton<ITaskManager, TaskManager>();
        serviceCollection.AddSingleton<ITaskRepository, TaskRepository>();
        serviceCollection.AddSingleton<ITaskView, TaskView>();

        // var _taskRepository = serviceCollection.BuildServiceProvider().GetService<ITaskRepository>();
        var _taskManager = serviceCollection.BuildServiceProvider().GetService<ITaskManager>();
        var _taskView = serviceCollection.BuildServiceProvider().GetService<ITaskView>();
        
        
        ResponsiblesRepository _responsiblesRepository = new();


        var newTask = new Tasks {Name = "TaskNameX", Description = "TaskDescription5", Responsible_Task = _responsiblesRepository.responsiblesList[0].Responsible_Name, TaskStatus = Status.TaskStatus.InProgress};
        // var newTask = new Tasks {};

        
        _taskManager.CreateNewTask(newTask);
        _taskView.ViewMyTasks();
        _taskView.ViewOthersTasks();
        _taskManager.ChangeResponsibleAtRandom();
        _taskManager.ChangeResponsibleSpecifically(1, 0);
        // _taskView.ViewMyTasks();
        // _taskView.ViewOthersTasks();
        


        Console.ReadKey();
    }
}
