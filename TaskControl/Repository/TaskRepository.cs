public class TaskRepository : ResponsiblesRepository, ITaskRepository
{
    public string logMessage;
    public List<Tasks> tasksList = new List<Tasks>();
    public TaskRepository()
    {
        tasksList.Add(new Tasks { Name = "TaskName1", Description = "TaskDescription1", 
                                Responsible_Task = responsiblesList[0].Responsible_Name, TaskStatus = Status.TaskStatus.Completed });

        tasksList.Add(new Tasks { Name = "TaskName2", Description = "TaskDescription2", 
                                Responsible_Task = responsiblesList[1].Responsible_Name, TaskStatus = Status.TaskStatus.InProgress });
        
        tasksList.Add(new Tasks { Name = "TaskName3", Description = "TaskDescription3", 
                                Responsible_Task = responsiblesList[2].Responsible_Name, TaskStatus = Status.TaskStatus.InProgress });

        tasksList.Add(new Tasks { Name = "TaskName4", Description = "TaskDescription4", 
                                Responsible_Task = responsiblesList[0].Responsible_Name, TaskStatus = Status.TaskStatus.Canceled });
    }
    
    
    public string RegisterTask(Tasks task)
    {
        tasksList.Add(task);
        int lastElement = tasksList.Count - 1;
        logMessage = $"{tasksList[lastElement].Name} registered with success!";
        System.Console.WriteLine($"\n{logMessage}\n");
        
        return logMessage;
    }
}