global using Microsoft.Extensions.DependencyInjection;


namespace TaskControlTest
{
    [TestClass]
    public class TaskManagerTest
    {
        private readonly Moq.Mock<ITaskRepository> _mockTaskRepository;
        private readonly Moq.Mock<ITaskManager> _mockTaskManager;
        private ITaskManager _taskManager;
        public TaskManagerTest()
        {
            _mockTaskRepository = new Moq.Mock<ITaskRepository>();
            _mockTaskManager = new Moq.Mock<ITaskManager>();
        }

        [TestMethod]
        public void CreateNewTaskTest()
        {
            var newTask = new Tasks {Name = "TaskNameX", Description = "TaskDescription5", Responsible_Task = "Luigi", TaskStatus = Status.TaskStatus.InProgress};
            _mockTaskRepository.Setup(repository => repository.RegisterTask(newTask));
            _taskManager = new TaskManager(_mockTaskRepository.Object);
            _taskManager.CreateNewTask(newTask);       
        }

        [TestMethod]
        public void NewTaskEmptyTest()
        {
            _mockTaskRepository.Setup(repository => repository.RegisterTask(null));
            _taskManager = new TaskManager(_mockTaskRepository.Object);

            try
            {
                _taskManager.CreateNewTask(null);
            }
            catch(Exception ex)
            {
                Assert.AreEqual("The task is invalid!", ex.Message);
            }
            
        }

        // [TestMethod]
        // public void NewTaskEmpty()
        // {
        //     var newTask = new Tasks {};
        //     _mockTaskRepository.Setup(repository => repository.RegisterTask(newTask));
        //     _taskManager = new TaskManager(_mockTaskRepository.Object);

        //     try
        //     {
        //         _taskManager.CreateNewTask(newTask);
        //     }
        //     catch(Exception ex)
        //     {
        //         Assert.AreEqual("Value cannot be null.", ex.Message);
        //     }
        // }



        

        [TestMethod]
        public void NameEmpty()
        {
            var newTask = new Tasks {Name = ""};
            _mockTaskRepository.Setup(repository => repository.RegisterTask(newTask));
            _taskManager = new TaskManager(_mockTaskRepository.Object);

            try
            {
                _taskManager.CreateNewTask(newTask);
            }
            catch(Exception ex)
            {
                Assert.AreEqual("The name of the task is invalid!", ex.Message);
            }
        }
        
        [TestMethod]
        public void NameLessThanTwoCharacters()
        {
            var newTask = new Tasks {Name = "Aa"};
            _mockTaskRepository.Setup(repository => repository.RegisterTask(newTask));
            _taskManager = new TaskManager(_mockTaskRepository.Object);

            try
            {
                _taskManager.CreateNewTask(newTask);
            }
            catch(Exception ex)
            {
                Assert.AreEqual("The name of the task is invalid!", ex.Message);
            }            
        }
        
        [TestMethod]
        public void DescriptionEmpty()
        {
            var newTask = new Tasks {Name = "TaskNameX", Description = null};
            _mockTaskRepository.Setup(repository => repository.RegisterTask(newTask));
            _taskManager = new TaskManager(_mockTaskRepository.Object);
            
            try
            {
                _taskManager.CreateNewTask(newTask);
            }
            catch(Exception ex)
            {
                Assert.AreEqual("The description of the task is invalid!", ex.Message);
            }
        }

        [TestMethod]
        public void Responsible_TaskEmpty()
        {
            var newTask = new Tasks {Name = "TaskNameX", Description = "Description", Responsible_Task = ""};
            _mockTaskRepository.Setup(repository => repository.RegisterTask(newTask));
            _taskManager = new TaskManager(_mockTaskRepository.Object);
            
            try
            {
                _taskManager.CreateNewTask(newTask);
            }
            catch(Exception ex)
            {
                Assert.AreEqual("The name of responsible is invalid!", ex.Message);
            }
        }
        
        [TestMethod]
        public void Responsible_TaskLessThanTwoCharacters()
        {
            var newTask = new Tasks {Name = "TaskNameX", Description = "Description", Responsible_Task = "Aa"};
            _mockTaskRepository.Setup(repository => repository.RegisterTask(newTask));
            _taskManager = new TaskManager(_mockTaskRepository.Object);
            
            try
            {
                _taskManager.CreateNewTask(newTask);
            }
            catch(Exception ex)
            {
                Assert.AreEqual("The name of responsible is invalid!", ex.Message);
            }   
        }
                
        [TestMethod]
        public void TaskStatusEmpty()
        {
            var newTask = new Tasks {Name = "TaskNameX", Description = "Description", Responsible_Task = "Luigi"};
            _mockTaskRepository.Setup(repository => repository.RegisterTask(newTask));
            _taskManager = new TaskManager(_mockTaskRepository.Object);
            try
            {
                _taskManager.CreateNewTask(newTask);
            }
            catch(Exception ex)
            {
                Assert.AreEqual("The status of the task is invalid!", ex.Message);
            }
        }

        [TestMethod]
        public void TaskStatusInProgress()
        {
            var newTask = new Tasks {Name = "TaskNameX", Description = "Description", Responsible_Task = "Luigi", TaskStatus = Status.TaskStatus.InProgress};
            _mockTaskRepository.Setup(repository => repository.RegisterTask(newTask));
            _taskManager = new TaskManager(_mockTaskRepository.Object);
            _taskManager.CreateNewTask(newTask);
            
            Assert.AreEqual("InProgress", newTask.TaskStatus.ToString());
        }

        [TestMethod]
        public void ChangeResponsibleSpecificallyTest()
        {
            _mockTaskManager.Setup(task => task.ChangeResponsibleSpecifically(0, 0));
            _taskManager = new TaskManager(_mockTaskRepository.Object);
            
            Assert.AreEqual("Luigi", _taskManager.ChangeResponsibleSpecifically(0, 0));
        }
    }
}
