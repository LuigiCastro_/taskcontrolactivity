namespace TaskControlTest;

[TestClass]
public class TaskRepositoryTest
{
    private readonly Moq.Mock<ITaskRepository> _mockTaskRepository;
    private ITaskRepository _taskRepository;
    public TaskRepositoryTest()
    {
        _mockTaskRepository = new Moq.Mock<ITaskRepository>();
    }

    [TestMethod]
    public void RegisterTaskTest()
    {
        var newTask = new Tasks {Name = "TaskNameX", Description = "TaskDescription5", Responsible_Task = "Luigi", TaskStatus = Status.TaskStatus.InProgress};
        _mockTaskRepository.Setup(repository => repository.RegisterTask(newTask));
        _taskRepository = new TaskManager(_mockTaskRepository.Object);
        _taskRepository.RegisterTask(newTask);
    }
}