namespace TaskControlTest;

[TestClass]
public class TaskViewTest
{
    private readonly Moq.Mock<ITaskView> _mockTaskView;
    private readonly Moq.Mock<ITaskRepository> _mockTaskRepository;
    private ITaskRepository _taskRepository;
    private ITaskView _taskView;
    public TaskViewTest()
    {
        _mockTaskView = new Moq.Mock<ITaskView>();
        _mockTaskRepository = new Moq.Mock<ITaskRepository>();
    }
    

    [TestMethod]
    public void ViewMyTasksTest()
    {
        TaskRepository _taskRepository = new();
        _mockTaskView.Setup(repository => repository.ViewMyTasks());
        _taskView = new TaskView(_mockTaskRepository.Object);

        Assert.AreEqual(_taskRepository.tasksList[3].Name, _taskView.ViewMyTasks());   
    }

    [TestMethod]
    public void ViewOtherTasksTest()
    {
        TaskRepository _taskRepository = new();
        _mockTaskView.Setup(repository => repository.ViewOthersTasks());
        _taskView = new TaskView(_mockTaskRepository.Object);

        Assert.AreEqual(_taskRepository.tasksList[2].Name, _taskView.ViewOthersTasks());   
    }      
}